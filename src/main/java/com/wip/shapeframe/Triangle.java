/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wip.shapeframe;

/**
 *
 * @author WIP
 */
public class Triangle extends Shape {
    private double b, h;
    private double a2, b2;
    
    public Triangle(double base, double height){
        super("Triangle");
        this.b = base;
        this.h = height;
    }

    public double getB() {
        return b;
    }

    public double getH() {
        return h;
    }

    public void setB(double b) {
        this.b = b;
    }

    public void setH(double h) {
        this.h = h;
    }
    //C
    public double findC(){
        this.a2 = b;
        this.b2 = h;
        return Math.sqrt((a2*a2)+(b2*b2));
    }
    
    @Override
    public double calArea() {
        return 0.5 * (b*h);
    }

    @Override
    public double calPerimeter() {
        return b + h + findC();
    }
    
}
