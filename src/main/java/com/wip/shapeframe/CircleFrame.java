/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wip.shapeframe;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

/**
 *
 * @author WIP
 */
public class CircleFrame extends JFrame {

    JLabel lbR;
    JTextField txtR;
    JButton btnCal;
    JLabel lbResult;

    public CircleFrame() {
        super("Circle");
        this.setSize(300, 300);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setLayout(null);

        lbR = new JLabel("radius:", JLabel.TRAILING);
        lbR.setSize(50, 20);
        lbR.setLocation(5, 5);
        lbR.setBackground(Color.WHITE);
        lbR.setOpaque(true);
        this.add(lbR);

        txtR = new JTextField();
        txtR.setSize(50, 20);
        txtR.setLocation(60, 5);
        this.add(txtR);

        btnCal = new JButton("Calculate");
        btnCal.setSize(100, 20);
        btnCal.setLocation(120, 5);
        this.add(btnCal);

        lbResult = new JLabel("Circle radius= xxx area= xxx perimeter= xxx");
        lbResult.setHorizontalAlignment(JLabel.CENTER);
        lbResult.setSize(300, 50);
        lbResult.setLocation(0, 50);
        lbResult.setBackground(Color.ORANGE);
        lbResult.setOpaque(true);
        this.add(lbResult);
        //Anonymous Class
        btnCal.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    //1. txtR -> strR (Sting)
                    String strR = txtR.getText();
                    //2. convert strR -> r: double parseDouble
                    double r = Double.parseDouble(strR); //-> NumberFormatExcepion
                    //3. instance obj Circlr(R) -> circle
                    Circle circle = new Circle(r);
                    //4. update lbResult from circle index to output
                    lbResult.setText("Circle r = " + String.format("%.2f", circle.getR())
                            + " area = " + String.format("%.2f", circle.calArea())
                            + " perimeter = " + String.format("%.2f", circle.calPerimeter()));
                } catch (Exception exc) {
                    JOptionPane.showMessageDialog(CircleFrame.this, "Error: Please input number!",
                            "Error", JOptionPane.ERROR_MESSAGE);
                    txtR.setText("");
                    txtR.requestFocus();
                }
            }

        });

    }

    public static void main(String[] args) {
        CircleFrame frame = new CircleFrame();

        frame.setVisible(true);
    }
}
