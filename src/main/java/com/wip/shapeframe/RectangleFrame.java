/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wip.shapeframe;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

/**
 *
 * @author WIP
 */
public class RectangleFrame extends JFrame {

    JLabel lbWidth, lbHeight;
    JTextField txtWidth, txtHeight;
    JButton btnCal;
    JLabel lbResult;

    public RectangleFrame() {
        super("Rectangle");
        this.setSize(300, 300);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setLayout(null);

        lbWidth = new JLabel("width :", JLabel.TRAILING);
        lbWidth.setSize(50, 20);
        lbWidth.setLocation(5, 5);
        lbWidth.setBackground(Color.RED);
        lbWidth.setOpaque(true);
        this.add(lbWidth);

        lbHeight = new JLabel("height :", JLabel.TRAILING);
        lbHeight.setSize(50, 20);
        lbHeight.setLocation(5, 30);
        lbHeight.setBackground(Color.CYAN);
        lbHeight.setOpaque(true);
        this.add(lbHeight);

        txtWidth = new JTextField();
        txtWidth.setSize(50, 20);
        txtWidth.setLocation(60, 5);
        this.add(txtWidth);

        txtHeight = new JTextField();
        txtHeight.setSize(50, 20);
        txtHeight.setLocation(60, 30);
        this.add(txtHeight);

        btnCal = new JButton("Calculate");
        btnCal.setSize(100, 30);
        btnCal.setLocation(120, 15);
        this.add(btnCal);

        lbResult = new JLabel("Rectangle w = xxx h = xxx area = xxx perimeter = xxx");
        lbResult.setHorizontalAlignment(JLabel.CENTER);
        lbResult.setSize(350, 50);
        lbResult.setLocation(0, 60);
        lbResult.setBackground(Color.YELLOW);
        lbResult.setOpaque(true);
        this.add(lbResult);
        //Anonymous Class
        btnCal.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    //1. txtR -> strR (Sting)
                    String strW = txtWidth.getText();
                    String strH = txtHeight.getText();
                    //2. convert strR -> r: double parseDouble
                    double w = Double.parseDouble(strW);
                    double h = Double.parseDouble(strH);
                    //3. instance obj Circlr(R) -> circle
                    Rectangle rectangle = new Rectangle(w, h);
                    //4. update lbResult from circle index to output
                    lbResult.setText("Rectangle w = " + String.format("%.2f", rectangle.getW())
                            + " h = " + String.format("%.2f", rectangle.getH())
                            + " area = " + String.format("%.2f", rectangle.calArea())
                            + " perimeter = " + String.format("%.2f", rectangle.calPerimeter()));
                } catch (Exception exc) {
                    JOptionPane.showMessageDialog(RectangleFrame.this, "Error: Please input number!",
                            "Error", JOptionPane.ERROR_MESSAGE);
                    txtWidth.setText("");
                    txtWidth.requestFocus();
                    txtHeight.setText("");
                    txtHeight.requestFocus();
                }
            }

        });
    }

    public static void main(String[] args) {
        RectangleFrame frame = new RectangleFrame();

        frame.setVisible(true);
    }
}
