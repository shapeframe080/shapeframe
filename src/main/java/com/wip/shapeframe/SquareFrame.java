/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wip.shapeframe;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

/**
 *
 * @author WIP
 */
public class SquareFrame extends JFrame {
    JLabel lbS;
    JTextField txtS;
    JButton btnCal;
    JLabel lbResult;
    
    public SquareFrame(){
        super("Square");
        this.setSize(300, 300);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setLayout(null);
        
        lbS = new JLabel("side :", JLabel.TRAILING);
        lbS.setSize(50, 20);
        lbS.setLocation(5, 5);
        lbS.setBackground(Color.MAGENTA);
        lbS.setOpaque(true);
        this.add(lbS);
        
        txtS = new JTextField();
        txtS.setSize(50, 20);
        txtS.setLocation(60, 5);
        this.add(txtS);
        
        btnCal = new JButton("Calculate");
        btnCal.setSize(100, 20);
        btnCal.setLocation(120, 5);
        this.add(btnCal);
        
        lbResult = new JLabel("Square side = xxx area = xxx perimeter = xxx");
        lbResult.setHorizontalAlignment(JLabel.CENTER);
        lbResult.setSize(300, 50);
        lbResult.setLocation(0, 50);
        lbResult.setBackground(Color.LIGHT_GRAY);
        lbResult.setOpaque(true);
        this.add(lbResult);
        
        //Anonymous Class
        btnCal.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    String strS = txtS.getText();
                    double s = Double.parseDouble(strS); 
                    Square square = new Square(s);
                    lbResult.setText("Square side = " + String.format("%.2f", square.getS())
                            + " area = " + String.format("%.2f", square.calArea())
                            + " perimeter = " + String.format("%.2f", square.calPerimeter()));
                } catch (Exception exc) {
                    JOptionPane.showMessageDialog(SquareFrame.this, "Error: Please input number!",
                            "Error", JOptionPane.ERROR_MESSAGE);
                    txtS.setText("");
                    txtS.requestFocus();
                }
            }

        });
        
    }
    public static void main(String[] args) {
        SquareFrame frame = new SquareFrame();
        
        frame.setVisible(true);
    }
    
}
