/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wip.shapeframe;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

/**
 *
 * @author WIP
 */
public class TriangleFrame extends JFrame {

    JLabel lbBase, lbHeight;
    JTextField txtBase, txtHeight;
    JButton btnCal;
    JLabel lbResult;

    public TriangleFrame() {
        super("Triangle");
        this.setSize(300, 300);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setLayout(null);
        
        lbBase = new JLabel("base :", JLabel.TRAILING);
        lbBase.setSize(50, 20);
        lbBase.setLocation(5, 5);
        lbBase.setBackground(Color.YELLOW);
        lbBase.setOpaque(true);
        this.add(lbBase);
        
        lbHeight = new JLabel("height :", JLabel.TRAILING);
        lbHeight.setSize(50, 20);
        lbHeight.setLocation(5, 30);
        lbHeight.setBackground(Color.orange);
        lbHeight.setOpaque(true);
        this.add(lbHeight);
        
        txtBase = new JTextField();
        txtBase.setSize(50, 20);
        txtBase.setLocation(60, 5);
        this.add(txtBase);
        
        txtHeight = new JTextField();
        txtHeight.setSize(50, 20);
        txtHeight.setLocation(60, 30);
        this.add(txtHeight);
        
        btnCal = new JButton("Calculate");
        btnCal.setSize(100, 30);
        btnCal.setLocation(120, 15);
        this.add(btnCal);
        
        lbResult = new JLabel("Triangle b = xxx h = xxx area = xxx perimeter = xxx");
        lbResult.setHorizontalAlignment(JLabel.CENTER);
        lbResult.setSize(350, 50);
        lbResult.setLocation(0, 60);
        lbResult.setBackground(Color.CYAN);
        lbResult.setOpaque(true);
        this.add(lbResult);
        //Anonymous Class
        btnCal.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    //1. txtR -> strR (Sting)
                    String strB = txtBase.getText();
                    String strH = txtHeight.getText();
                    //2. convert strR -> r: double parseDouble
                    double b = Double.parseDouble(strB);
                    double h = Double.parseDouble(strH);
                    //3. instance obj Circlr(R) -> circle
                    Triangle triangle = new Triangle(b, h);
                    //4. update lbResult from circle index to output
                    lbResult.setText("Triangle b = " + String.format("%.2f", triangle.getB())
                            + " h = " + String.format("%.2f", triangle.getH())
                            + " area = " + String.format("%.2f", triangle.calArea())
                            + " perimeter = " + String.format("%.2f", triangle.calPerimeter()));
                } catch (Exception exc) {
                    JOptionPane.showMessageDialog(TriangleFrame.this, "Error: Please input number!",
                            "Error", JOptionPane.ERROR_MESSAGE);
                    txtBase.setText("");
                    txtBase.requestFocus();
                    txtHeight.setText("");
                    txtHeight.requestFocus();
                }
            }

        });
    }

    public static void main(String[] args) {
        TriangleFrame frame = new TriangleFrame();

        frame.setVisible(true);
    }
}
